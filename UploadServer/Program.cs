﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using JWT;
using JWT.Algorithms;
using JWT.Builder;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace UploadServer
{
    public class Program
    {
        public static void Main(string[] args)
        {
            /* payload
                 * {
                 *     exp:'',
                 *     size:'',
                 *     app:'',
                 *     exts:'.jpeg;.docx;'
                 */
//            //生成token
//            var token = new JwtBuilder()
//                .WithAlgorithm(new HMACSHA256Algorithm())
//                .WithSecret("1234561234")
//                .AddClaim("exp", DateTimeOffset.UtcNow.AddYears(1).ToUnixTimeSeconds())
//                .AddClaim("size", "2mb")
//                .AddClaim("app", "test")
//                .Build();
//            Console.WriteLine(token);

            CreateWebHostBuilder(args).Build().Run();
        }

        private static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseConfiguration(new ConfigurationBuilder().SetBasePath(Environment.CurrentDirectory)
                    .AddJsonFile("appsettings.json").Build())
                .ConfigureKestrel(options => { options.Limits.KeepAliveTimeout = TimeSpan.FromMinutes(10); })
                .UseStartup<Startup>();
    }
}